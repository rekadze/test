require 'active_support'
require 'rspec'
require 'rspec/its'
require 'rspec/collection_matchers'
require 'capybara'
require 'capybara/rspec'
require 'selenium-webdriver'
require 'site_prism'

ActiveSupport::Dependencies.autoload_paths << Dir[File.dirname(__FILE__) + '/pages/']
Dir[File.dirname(__FILE__) + '/support/**/*.rb'].each { |f| require f }

Capybara.configure do |config|
  config.run_server = false
  config.default_selector = :css
  config.default_wait_time = 40
  config.ignore_hidden_elements = true
  config.match = :prefer_exact
  config.visible_text_only = true
  config.default_driver = :selenium
  config.app_host = 'http://www.lazada.vn'
end

RSpec.configure do |config|
  config.order = :random

  config.expect_with :rspec do |expectations|
    expectations.syntax = [:should, :expect]
  end

  config.mock_with :rspec do |mocks|
    mocks.syntax = [:should, :expect]
    mocks.verify_partial_doubles = true
  end

end
