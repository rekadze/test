module Features
  module FeaturesHelpers

    def with_hidden_elements
      orig = Capybara.ignore_hidden_elements
      Capybara.ignore_hidden_elements = false
      yield
    ensure
      Capybara.ignore_hidden_elements = orig
    end

    def reload_page
      visit page.driver.browser.current_url
    end

  end
end