class Sections::HeaderSection < SitePrism::Section

  element :login_link, :xpath, "//li[contains(@class,'header__user__account')]/span"
  element :sign_up_button, "#newsletter-btn"
  element :cart_popup_link, ".header__cart"
  section :login_regisrtation_section, "#popup-form-user-login" do
    element :registration_link, "#sign-up-now"
  end
  elements :brands_list, ".header__brands__list li a"
end