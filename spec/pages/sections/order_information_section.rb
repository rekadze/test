class Sections::OrderInformationSection < SitePrism::Section
  section :order_table_section, ".order_scroll_table tbody tr" do
    element :product_name_value, ".product"
    element :qty_value, ".qty"
    element :product_cost_value, ".right_align"
    element :delivery_date_value, :xpath, "//*[@class='product']/.."
  end
end