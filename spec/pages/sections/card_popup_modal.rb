class Sections::CardPopupModal < SitePrism::Page

  sections :product_section, ".producttable" do
    element :product_name_value, ".productdescription"
    element :product_cost_value, :xpath, "//td[contains(@class, 'price center')]"
    element :product_total_cost_value, :xpath, "//td[contains(@class, 'price lastcolumn')]"
    element :item_qty_select, ".cart-product-item-cell-qty-select"
  end
  element :total_cost_value, :xpath, "//*[@class='total']/td/div[1]"
  element :buy_button, ".submit_btn"

  def change_qty_for product_price, qty
    product_section.each do |product|
      if product.product_cost_value.text.gsub(/[.]/,'').to_f.equal? product_price
        product.item_qty_select.find(:option, qty).select_option
      end
    end
  end
end