class SignupPage < SitePrism::Page
  set_url "/customer/account/create/"
  set_url_matcher "/customer/account/create/"

  section :header_section, Sections::HeaderSection, ".header"

  element :gender_select, "#RegistrationForm_gender"
  element :email_field, "#RegistrationForm_email"
  element :name_field, "#RegistrationForm_first_name"
  element :date_of_birth_year_select, "#RegistrationForm_year"
  element :date_of_birth_month_select, "#RegistrationForm_month"
  element :date_of_birth_day_select, "#RegistrationForm_day"
  element :password_field, "#RegistrationForm_password"
  element :password_confirmation, "#RegistrationForm_password2"
  element :submit_button, "#send"
  element :newsletter_subscribed_checkbox, "#RegistrationForm_is_newsletter_subscribed"
  element :privacy_policy_link, :xpath, "//a[text()='chính sách']"
  element :error_message, '.s-error'
end