class Order::DeliveryInformationPage < SitePrism::Page
  set_url "checkout/step/deliveryinformation/"

  element :city_select, "#ThreeStepShippingAddressForm_location_0"
  element :district_select, "#ThreeStepShippingAddressForm_location_1"
  element :address_field, "#ThreeStepShippingAddressForm_address1"
  element :continue_button, ".submit_btn_text"

  section :order_information_section, Sections::OrderInformationSection, ".order_sum_container"
end