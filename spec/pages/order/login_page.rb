class Order::LoginPage < SitePrism::Page
  set_url "/checkout/step/index"

  element :email_field, "#EmailLoginForm_email"
  element :guest_radio_button, :xpath, "//input[@id='EmailLoginForm_isGuestCheckout' and @value='1']"
  element :user_radio_button, :xpath, "//input[@id='EmailLoginForm_isGuestCheckout' and @value='0']"
  element :password_field, "#EmailLoginForm_password"
  element :continue_button, ".submit_btn_text"

  section :order_information_section, Sections::OrderInformationSection, ".order_sum_container"
end