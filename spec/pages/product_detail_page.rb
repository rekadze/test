class ProductDetailPage < SitePrism::Page
  set_url "/hang-nhap-khau-apple-iphone-6-47-8mp-16gb-gold-240455.html"
  set_url_matcher "/hang-nhap-khau-apple-iphone-6-47-8mp-16gb-gold-240455.html"

  element :product_name_value, "#prod_title"
  element :product_cost_value, "#special_price_box"
  section :delivery_information_section, ".delivery-types" do
    element :delivery_province_select, "#delivery_province"
    element :delivery_zip_select, "#delivery_zip"
    element :delivery_data_value, ".delivery-types__data-list-item"
  end

  element :add_to_card_button, "#AddToCart"
  section :header_section, Sections::HeaderSection, ".header"
end