class HomePage < SitePrism::Page
  set_url "/"
  set_url_matcher "/"

  section :header_section, Sections::HeaderSection, ".header"
  sections :product_card_section, "a.product-card" do
    element :buy_button_link, ".button-buy"
    element :product_name_value, ".product-card__name"
    element :product_price_value, ".product-card__price"
  end
  elements :product_detail_link, "a.product-card"
end