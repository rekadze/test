feature 'Validate product information (name/price/delivery days) between product detail page and cart page' do
  let(:product_detail_page) { ProductDetailPage.new }
  let(:card_popup_modal)  { Sections::CardPopupModal.new }
  let(:order_login_page)  { Order::LoginPage.new }
  let(:order_delivery_information_page)  { Order::DeliveryInformationPage.new }

  scenario "Product information must be identical between product detail page and cart page" do
    attrs = {province: "Kon Tum", zip: "Huyện Kon Rẫy", email: "test@test.com"}
    product_detail_page.load
    p_name = product_detail_page.product_name_value.text
    p_cost = product_detail_page.product_cost_value.text
    product_detail_page.add_to_card_button.click
    within (".nyroModalCont") do
      expect(card_popup_modal.product_section[0].product_name_value.text).to include p_name
      expect(card_popup_modal.product_section[0].product_cost_value.text).to include p_cost
      expect(card_popup_modal.product_section[0].product_total_cost_value.text).to include p_cost
      card_popup_modal.buy_button.click
    end
    order_login_page.email_field.set attrs[:email]
    order_login_page.continue_button.click
    order_delivery_information_page.wait_for_city_select
    order_delivery_information_page.city_select.find(:option, attrs[:province]).select_option
    order_delivery_information_page.wait_for_district_select
    order_delivery_information_page.district_select.find(:option, attrs[:zip]).select_option
    order_delivery_information_page.order_information_section.order_table_section.wait_for_delivery_date_value
    expect(order_delivery_information_page.order_information_section.order_table_section.product_name_value.text).to include p_name
    expect(p_cost).to include order_delivery_information_page.order_information_section.order_table_section.product_cost_value.text
    expect(order_delivery_information_page.order_information_section.order_table_section.delivery_date_value).to be_present
  end
end