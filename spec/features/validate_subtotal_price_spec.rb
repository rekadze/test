feature 'Validate subtotal price when change qty in cart popup' do
  let(:home_page)    { HomePage.new }
  let(:product_detail_page) { ProductDetailPage.new }
  let(:card_popup_modal)  { Sections::CardPopupModal.new }

  scenario "Validate subtotal price when change qty in cart popup" do
    home_page.load
    home_page.header_section.brands_list.first.click
    first_price = home_page.product_card_section.first.product_price_value.text.gsub(/[.]/,'').to_f
    p first_price
    home_page.product_detail_link.first.click
    product_detail_page.add_to_card_button.click
    home_page.header_section.brands_list.last.click
    second_price = home_page.product_card_section.first.product_price_value.text.gsub(/[.]/,'').to_f
    p second_price
    home_page.product_detail_link.first.click
    product_detail_page.add_to_card_button.click
    within (".nyroModalCont") do
      expect(card_popup_modal.total_cost_value.text.gsub(/[.]/,'').to_f).to eq first_price+second_price
      card_popup_modal.change_qty_for(first_price, "2")
      expect(card_popup_modal.total_cost_value.text.gsub(/[.]/,'').to_f).to eq 2*first_price+second_price
    end
  end
end