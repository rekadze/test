feature 'Validate error message in register new account form' do
  let(:home_page)    { HomePage.new }
  let(:signup_page)  { SignupPage.new }

  scenario "User can't signup without confirmation password" do
    home_page.load
    home_page.header_section.login_link.click
    home_page.header_section.wait_for_login_regisrtation_section
    home_page.header_section.login_regisrtation_section.registration_link.click
    signup_page.load
    signup_page.wait_for_gender_select
    attrs = {gender: 'Nam', email: 'test@email.com', name: 'Test', year: '1990', month: 'August', day: '21', password: 'Test12#45'}
    signup_page.gender_select.find(:option, attrs[:gender]).select_option
    signup_page.email_field.set attrs[:email]
    signup_page.name_field.set attrs[:name]
    signup_page.date_of_birth_year_select.find(:option, attrs[:year]).select_option
    signup_page.date_of_birth_month_select.find(:option, attrs[:month]).select_option
    signup_page.wait_for_date_of_birth_day_select
    signup_page.date_of_birth_day_select.find(:option, attrs[:day]).select_option
    signup_page.password_field.set attrs[:password]
    signup_page.password_confirmation attrs[:confirm_password]
    signup_page.submit_button.click
    signup_page.wait_for_error_message
    expect(signup_page.error_message.text).to include('Mật khẩu phải có ít nhất 1 chữ số')
  end

end